import { AppProps } from 'next/app';
import Head from 'next/head';
import { FC } from 'react';
import '../styles/globals.css';

const MyApp: FC<AppProps> = ({ Component, pageProps }): JSX.Element => {
  return (
    <>
      <Head>
        <title>MyTop - лучший топ</title>
      </Head>
      <Component {...pageProps} />
    </>
  );
};

export default MyApp;
