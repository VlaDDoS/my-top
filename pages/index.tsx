import { HTag } from '../components';
import { withLayout } from '../layout/Layout';

const Home = (): JSX.Element => {
  return (
    <>
      <HTag tag="h3">Hello Next.js</HTag>
    </>
  );
};

export default withLayout(Home);
