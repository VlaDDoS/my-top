import React, { FC } from 'react';
import { FooterProps } from './Footer.props';
import styles from './Footer.module.css';
import cn from 'classnames';

export const Footer: FC<FooterProps> = ({
  className,
  ...props
}): JSX.Element => {
  return (
    <footer {...props} className={cn(styles.footer, className)}>
      <small className={styles.link}>
        OwlTop © 2020 - {new Date().getFullYear()} Все права защищены
      </small>
      <a href="#" target="_blank">
        Пользовательское соглашение
      </a>
      <a href="#" target="_blank">
        Политика конфиденциальности
      </a>
    </footer>
  );
};
