import React, { FC } from 'react';
import { HTagProps } from './HTag.props';
import styles from './HTag.module.css';

export const HTag: FC<HTagProps> = ({ tag, children }): JSX.Element => {
  const { h1, h2, h3 } = styles;

  switch (tag) {
    case 'h1':
      return <h1 className={h1}>{children}</h1>;
    case 'h2':
      return <h2 className={h2}>{children}</h2>;
    case 'h3':
      return <h3 className={h3}>{children}</h3>;
    default:
      return <></>;
  }
};
