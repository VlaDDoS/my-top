import { DetailedHTMLProps, HTMLAttributes, ReactNode } from 'react';

export interface PTagProps
  extends DetailedHTMLProps<
    HTMLAttributes<HTMLParagraphElement>,
    HTMLParagraphElement
  > {
  fontSize?: 'small' | 'medium' | 'large';
  children: ReactNode;
}
