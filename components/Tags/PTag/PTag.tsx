import React, { FC } from 'react';
import { PTagProps } from './PTag.props';
import cn from 'classnames';
import styles from './PTag.module.css';

export const PTag: FC<PTagProps> = ({
  fontSize = 'medium',
  children,
  className,
  ...props
}): JSX.Element => {
  return (
    <p
      className={cn(styles.p, className, {
        [styles.small]: fontSize === 'small',
        [styles.medium]: fontSize === 'medium',
        [styles.large]: fontSize === 'large',
      })}
      {...props}
    >
      {children}
    </p>
  );
};
