import { DetailedHTMLProps, HTMLAttributes, ReactNode } from 'react';

export interface TagProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLSpanElement>, HTMLSpanElement> {
  size?: 'small' | 'medium';
  color?: 'ghost' | 'red' | 'gray' | 'green' | 'primary';
  children: ReactNode;
  href?: string;
}
