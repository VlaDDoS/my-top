import React, { FC, KeyboardEvent, useEffect, useState } from 'react';
import { MyRatingProps } from './MyRating.props';
import styles from './MyRating.module.css';
import cn from 'classnames';
import StarIcon from './star.svg';

export const MyRating: FC<MyRatingProps> = ({
  rating,
  setRating,
  isEditable = false,
  ...props
}): JSX.Element => {
  const [ratingArray, setRatingArray] = useState<JSX.Element[]>(
    Array(5).fill(<></>)
  );

  const changeDisplay = (idx: number) => {
    if (!isEditable) {
      return;
    }

    constuctRating(idx);
  };

  const handleClick = (idx: number) => {
    if (!isEditable || !setRating) {
      return;
    }

    setRating(idx);
  };

  const handleSpace = (idx: number, event: KeyboardEvent<SVGAElement>) => {
    if (event.code !== 'Space' || !setRating) {
      return;
    }

    setRating(idx);
  };

  const constuctRating = (currentRating: number) => {
    const updatedArray = ratingArray.map((r: JSX.Element, idx: number) => {
      return (
        <span
          className={cn(styles.star, {
            [styles.filled]: idx < currentRating,
            [styles.editable]: isEditable,
          })}
          onMouseEnter={() => changeDisplay(idx + 1)}
          onMouseLeave={() => changeDisplay(rating)}
          onClick={() => handleClick(idx + 1)}
        >
          <StarIcon
            tabIndex={isEditable ? 0 : -1}
            onKeyDown={(event: KeyboardEvent<SVGAElement>) =>
              isEditable && handleSpace(idx + 1, event)
            }
          />
        </span>
      );
    });

    setRatingArray(updatedArray);
  };

  useEffect(() => {
    constuctRating(rating);
  }, [rating]);

  return (
    <div {...props}>
      {ratingArray.map((r, idx) => (
        <span key={idx}>{r}</span>
      ))}
    </div>
  );
};
