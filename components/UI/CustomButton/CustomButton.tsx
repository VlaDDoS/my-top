import React, { FC } from 'react';
import ArrowIcon from './arrow.svg';
import { CustomButtonProps } from './CustomButton.props';
import cn from 'classnames';
import styles from './CustomButton.module.css';

export const CustomButton: FC<CustomButtonProps> = ({
  appearance,
  children,
  className,
  arrow = 'none',
  ...props
}): JSX.Element => {
  return (
    <button
      className={cn(styles.button, className, {
        [styles.primary]: appearance === 'primary',
        [styles.ghost]: appearance === 'ghost',
      })}
      {...props}
    >
      {children}
      {arrow !== 'none' && (
        <span
          className={cn(styles.arrow, {
            [styles.down]: arrow === 'down',
          })}
        >
          <ArrowIcon />
        </span>
      )}
    </button>
  );
};
